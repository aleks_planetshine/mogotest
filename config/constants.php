<?php
return [

	/*
	|--------------------------------------------------------------------------
	| Total teams count
	|--------------------------------------------------------------------------
	|
	| Here you can specify tournament total team count
	|
	*/

	'teams_count' => 16,
];