<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'tournament.index', 'uses' => 'TournamentController@index']);
Route::post('/start', ['as' => 'tournament.start', 'uses' => 'TournamentController@start']);
Route::post('/generate-group/{group_id}', ['as' => 'tournament.generate_group', 'uses' => 'TournamentController@generate_group']);