To setup project execute following commands:
- composer install
- cp .env.example .env
- php artisan migrate
- php artisan serve

P.S. configure database settings in .env file according to your environment