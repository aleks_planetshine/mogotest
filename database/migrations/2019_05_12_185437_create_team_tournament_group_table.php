<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamTournamentGroupTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Enable foreign key support for SQLite
		Schema::enableForeignKeyConstraints();

		Schema::create('team_tournament_group', function (Blueprint $table) {
			$table->integer('team_id')->unsigned();
			$table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
			$table->integer('tournament_id')->unsigned();
			$table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');
			$table->integer('group_id')->unsigned();
			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_tournament_group', function (Blueprint $table) {
			$table->dropForeign(['team_id']);
			$table->dropForeign(['tournament_id']);
			$table->dropForeign(['group_id']);
		});

		Schema::dropIfExists('team_tournament_group');
	}
}
