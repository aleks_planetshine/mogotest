<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// Enable foreign key support for SQLite
		Schema::enableForeignKeyConstraints();

        Schema::create('standings', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('tournament_id')->unsigned();
			$table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');
			$table->integer('group_id')->unsigned()->nullable();
			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
			$table->integer('winner_id')->unsigned()->nullable();
			$table->foreign('winner_id')->references('id')->on('teams')->onDelete('cascade');
			// 16 - regular standings, 4 - quoter finals, 2 - semi finals, 1 - finals
			$table->tinyInteger('round')->default(16);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('standings', function (Blueprint $table) {
			$table->dropForeign(['winner_id']);
			$table->dropForeign(['tournament_id']);
		});

        Schema::dropIfExists('standings');
    }
}
