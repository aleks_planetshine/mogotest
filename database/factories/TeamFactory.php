<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Team;
use Faker\Generator as Faker;

// default test values for Team model
$factory->define(Team::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
    ];
});
