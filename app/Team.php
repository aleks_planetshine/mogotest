<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $fillable = [
		'name',
	];

	/**
	 *  Define relationship with Tournament model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function tournaments() {
		return $this->belongsToMany('App\Tournament', 'team_tournament_group');
	}

	/**
	 *  Define relationship with Group model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function groups() {
		return $this->belongsToMany('App\Group', 'team_tournament_group');
	}

	/**
	 *  Define relationship with Standing model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function standings() {
		return $this->belongsToMany('App\Standing');
	}

	/**
	 * Define TeamTournamentGroup model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function teamsTournamentsGroups()
	{
		return $this->hasMany('App\TeamTournamentGroup');
	}

}
