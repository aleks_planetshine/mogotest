<?php

namespace App\Http\Controllers;

use Config;
use App\Tournament;
use App\Team;
use App\Group;
use Illuminate\Support\Facades\DB;

class TournamentController extends Controller {
	protected $teams_count, $teams_per_group;

	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->teams_count = Config::get('constants.teams_count');
		$this->teams_per_group = ceil($this->teams_count / 2);
	}


	/**
	 * @todo handle playoff rounds
	 *
	 * @param Tournament $tournament
	 * @param            $round
	 *
	 * @return array
	 */
	protected function getPlayoffRoundTeams(Tournament $tournament, $round) {
		$source_round = $round * $round;
		if( $source_round == 1 ) $source_round = 2;

		if( $source_round == 16 ) {
			$teams_tournaments_groups = $tournament->teamsTournamentsGroups()
				->with('group')
				->distinct('group_id')
				->select('group_id')
				->get();

			$regular_winners = [];

			foreach( $teams_tournaments_groups as $ttg ) {
				$regular_winners[ $ttg->group->name ] = $tournament->standings()
					->where('group_id', $ttg->group->id)
					->select('winner_id', DB::raw('count(*) as win_count'))
					->groupBy('winner_id')
					->orderBy('win_count', 'desc')
					->limit(4)
					->get();
			}

			return $regular_winners;
		} else {
			return [];
		}

	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		$tournament = Tournament::where('status', 1)->first();
		if( is_null($tournament) ) {
			return view('tournament.index')->with('tournament_active', false);
		}

		$standings_regular = $tournament->standings()->where('round', 16)->with(['group', 'teams', 'winner'])->get();

		// group standings
		$standings_by_group = [];
		$groups_generate = [];
		$playoff_started = true;
		foreach( $standings_regular as $st ) {
			if( !isset($standings_by_group[ $st->group->name ]) ) {
				$standings_by_group[ $st->group->name ] = [$st];
				$groups_generate[ $st->group->name ] = ['status' => false, 'group_id' => $st->group->id];
			} else {
				$standings_by_group[ $st->group->name ][] = $st;
			}

			// detect if we
			if( is_null($st->winner_id) ) {
				$groups_generate[ $st->group->name ]['status'] = true;
				$playoff_started = false;
			}
		}

		// chunk each group standings by game count
		foreach( $standings_by_group as $key => $gs ) {
			$standings_by_group[ $key ] = array_chunk($gs, $this->teams_per_group - 1);
		}

		$teams_tournaments_groups = $tournament->teamsTournamentsGroups()->with(['group', 'team'])->get();

		// group teams
		$teams_names_by_group = [];
		foreach( $teams_tournaments_groups as $ttg ) {
			if( !isset($teams_names_by_group[ $ttg->group->name ]) ) $teams_names_by_group[ $ttg->group->name ] = [$ttg->team->name];
			else $teams_names_by_group[ $ttg->group->name ][] = $ttg->team->name;
		}

		$playoff_standings = $playoff_started ? $tournament->standings()->where('round', 4)->with(['teams'])->get() : false;

		return view('tournament.index')
			->with('tournament_active', true)
			->with('standings_by_group', $standings_by_group)
			->with('teams_names_by_group', $teams_names_by_group)
			->with('groups_generate', $groups_generate)
			->with('playoff_standings', $playoff_standings);
	}


	/**
	 * @todo fill tournament name column using user input
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function start() {
		// continue only if no tournament is in progress
		if( !is_null(Tournament::where('status', 1)->first()) ) {
			return redirect()->route('tournament.index')->with('error', 'Current tournament is not finished!');
		}

		// get random teams
		$teams = Team::inRandomOrder()->limit($this->teams_count)->get();

		if( is_null($teams) || $teams->count() < $this->teams_count ) {
			return redirect()->route('tournament.index')->with('error', 'Not enough teams to start tournament!');
		}

		$tournament = Tournament::create();

		$group_a = Group::firstOrCreate(['name' => 'A']);
		$group_b = Group::firstOrCreate(['name' => 'B']);

		/**
		 * insert tournament_team_group entries for each team
		 */
		$teams_ids_chunked = array_chunk(array_column($teams->toArray(), 'id'), $this->teams_per_group);

		$groups = $group_a_teams = $group_b_teams = [];

		// assign A division teams
		foreach( $teams_ids_chunked[0] as $at ) {
			$groups[ $at ] = ['group_id' => $group_a->id];
			$group_a_teams[] = $at;
		}

		// assign B division teams
		foreach( $teams_ids_chunked[1] as $bt ) {
			$groups[ $bt ] = ['group_id' => $group_b->id];
			$group_b_teams[] = $bt;
		}

		$tournament->teams()->attach($groups);

		/**
		 * insert standings initial data
		 */
		$standings_a_instances = $standings_b_instances = [];
		$regular_standings_count = $this->teams_per_group * ($this->teams_per_group - 1);

		// group A regular standings
		for( $st = 0; $st < $regular_standings_count; $st++ ) {
			$standings_a_instances[] = $tournament->standings()->create(['group_id' => $group_a->id]);
		}

		// group B regular standings
		for( $st = 0; $st < $regular_standings_count; $st++ ) {
			$standings_b_instances[] = $tournament->standings()->create(['group_id' => $group_b->id]);
		}

		// quarter finals
		for( $st_quarter = 0; $st_quarter < 4; $st_quarter++ ) {
			$tournament->standings()->create(['round' => 4]);
		}

		// semi finals
		for( $st_semi = 0; $st_semi < 2; $st_semi++ ) {
			$tournament->standings()->create(['round' => 2]);
		}

		// finals
		$tournament->standings()->create(['round' => 1]);


		/**
		 * insert teams for each standing
		 */
		$st_counter = 0;

		// group A regular standings teams
		foreach( $group_a_teams as $team_1 ) {
			reset($group_a_teams);

			foreach( $group_a_teams as $team_2 ) {
				if( $team_1 == $team_2 ) continue;

				$standings_a_instances[ $st_counter ]->teams()->attach([$team_1, $team_2]);
				$st_counter++;
			}
		}

		$st_counter = 0;

		// group B regular standings teams
		foreach( $group_b_teams as $team_1 ) {
			reset($group_b_teams);

			foreach( $group_b_teams as $team_2 ) {
				if( $team_1 == $team_2 ) continue;

				$standings_b_instances[ $st_counter ]->teams()->attach([$team_1, $team_2]);
				$st_counter++;
			}
		}


		return redirect()->route('tournament.index')->with('success', 'Tournament created!');
	}

	/***
	 * @param $group_id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function generate_group($group_id) {
		$tournament = Tournament::where('status', 1)->firstOrFail();
		$group_standings = $tournament->standings()->where('group_id', $group_id)->with(['teams'])->get();

		foreach( $group_standings as $standing ) {
			$teams_ids = [$standing->teams->first()->id, $standing->teams->last()->id];
			$rand_key = array_rand($teams_ids);
			$standing->winner_id = $teams_ids[ $rand_key ];
			$standing->save();
		}

		$other_standings = $tournament->standings()->whereNull('winner_id')->where('group_id', '!=', $group_id)->with(['teams'])->get();

		// check if we need to process playoff standings
		if( $other_standings->isEmpty() ) {
			$teams_grouped = $this->getPlayoffRoundTeams($tournament, 4);

			$quarter_final_standings = $tournament->standings()->with('teams')->where('round', 4)->get();

			$teams_group_b = array_reverse($teams_grouped['B']->toArray());

			foreach( $teams_grouped['A'] as $index => $a_group_team ) {
				$current_standing = [$a_group_team->winner_id, $teams_group_b[ $index ]['winner_id']];
				$quarter_final_standings[ $index ]->teams()->attach($current_standing);
			}
		}

		return redirect()->route('tournament.index')->with('success', 'Group standings generated!');
	}
}
