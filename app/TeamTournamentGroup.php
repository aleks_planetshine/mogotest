<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Team;
use App\Tournament;
use App\Group;

class TeamTournamentGroup extends Model
{
	protected $table = 'team_tournament_group';

	/**
	 * Define relationship with Team model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function team()
	{
		return $this->belongsTo('App\Team');
	}

	/**
	 * Define relationship with Tournament model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tournament()
	{
		return $this->belongsTo('App\Tournament');
	}

	/**
	 * Define relationship with Group model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function group()
	{
		return $this->belongsTo('App\Group');
	}

}
