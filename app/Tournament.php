<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
	/**
	 *  Define relationship with Team model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function teams() {
		return $this->belongsToMany('App\Team', 'team_tournament_group');
	}


	/**
	 *  Define relationship with Standings model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function standings() {
		return $this->hasMany('App\Standing');
	}

	/**
	 * Define TeamTournamentGroup model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function teamsTournamentsGroups()
	{
		return $this->hasMany('App\TeamTournamentGroup');
	}
}
