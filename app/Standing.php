<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standing extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *ass
	 * @var array
	 */
	protected $fillable = [
		'tournament_id', 'group_id', 'winner_id', 'round', 'completed'
	];

	/**
	 * Define relationship with Team model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function teams() {
		return $this->belongsToMany('App\Team');
	}

	/**
	 * Define relationship with Team model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function winner() {
		return $this->belongsTo('App\Team');
	}

	/**
	 * Define relationship with Tournament model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tournament() {
		return $this->belongsTo('App\Tournament');
	}

	/**
	 * Define relationship with Group model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function group() {
		return $this->belongsTo('App\Group');
	}
}
