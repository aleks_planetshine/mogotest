<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *ass
	 * @var array
	 */
	protected $fillable = [
		'name'
	];

	/**
	 *  Define relationship with Team model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function teams() {
		return $this->belongsToMany('App\Team', 'tournament_team_group');
	}

	/**
	 *  Define relationship with Tournament model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function tournaments() {
		return $this->belongsToMany('App\Team', 'tournament_team_group');
	}

	/**
	 *  Define relationship with Standings model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function standings() {
		return $this->hasMany('App\Standing');
	}

	/**
	 * Define TeamTournamentGroup model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function teamsTournamentsGroups()
	{
		return $this->hasMany('App\TeamTournamentGroup');
	}
}
