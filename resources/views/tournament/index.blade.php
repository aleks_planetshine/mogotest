@extends('layouts.app')

@section('content')
	@if( !$tournament_active )
		<form action="{{ route('tournament.start') }}" method="POST" class="mt-5 text-center">
			@csrf
			<h2>Currently there is no active tournaments</h2>
			<button type="submit" class="btn btn-primary">Start tournament</button>
		</form>
	@else
		@foreach($standings_by_group as $group_name => $rows)
			<div class="group">
				<h2 class="mt-5">{{ $group_name }}</h2>

				<div class="row">
					<div class="col">&nbsp;</div>
					@foreach($teams_names_by_group[$group_name] as $team_name)
						<div class="col text-center">{{ $team_name }}</div>
					@endforeach
				</div>

				@foreach($rows as $row_index => $row)
					<div class="row">
						<div class="col">{{ $teams_names_by_group[$group_name][$row_index] }}</div>

						@foreach($row as $col_index => $col)
							@if($row_index == $col_index)
								<div class="col" style="background: grey;">&nbsp;</div>
							@endif
							<div class="col">
								@if( !empty($col->winner) )
									@if($col->winner->id == $col->teams->first()->id)
										<div class="col text-center">1:0</div>
									@else
										<div class="col text-center">0:1</div>
									@endif
								@else
									<div class="col">&nbsp;</div>
								@endif
							</div>
						@endforeach

						@if ($loop->last)
							<div class="col" style="background: grey;">&nbsp;</div>
						@endif
					</div>
				@endforeach

				@if($groups_generate[$group_name]['status'])
					<form action="{{ route('tournament.generate_group', $groups_generate[$group_name]['group_id']) }}" method="POST" class="mt-5">
						@csrf
						<button type="submit" class="btn btn-primary">Generate group results</button>
					</form>
				@endif
			</div>
		@endforeach

		@if( !empty($playoff_standings) )
			<div class="group">
				<h2 class="mt-5">Playoff quarter finals</h2>
				<ul>
					@foreach($playoff_standings as $standing)
						<li>{{ $standing->teams->first()->name }} / {{ $standing->teams->last()->name }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	@endif

@endsection